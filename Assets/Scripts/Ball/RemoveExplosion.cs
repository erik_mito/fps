﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class RemoveExplosion : MonoBehaviour {
    private float timeToRemove = 2.0f;
    private float timeElapsed = 0;
   
   void Awake(){
       timeToRemove = 2.0f;
   }
   
    // Update is called once per frame
    void Update () {
        timeElapsed += Time.deltaTime;
        if (timeElapsed > timeToRemove) {
            Destroy (gameObject);
        }
    }
}